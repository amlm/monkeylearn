# Example projects using MonkeyLearn

[MonkyLearn](http://monkeylearn.com) makes it easy for developers to 
create customized text analysis models using unstructured data. They 
provide an API for you to perform entity extraction, keyword extraction,
 language detection, topic detection and sentiment analysis.

The project we're working on at Avon Machine Learning Meetup is simply 
 to recreate the example outlined in the three part blog post on the
 [MonkeyLearn Blog](https://blog.monkeylearn.com/filtering-startup-news-machine-learning/).

The goal of the project is to introduce you to a service that can provide
you with useful results from a large amount of unstructured text 
without burdening you with having to learn the math behind the processes.
 In particular, the project introduces you to Scrapy, a web scraping 
 library written in Python, which you use to gather a large amount of 
 technology related news articles from three popular websites. It then
 shows you how to use MonkeyLearn to answer the following questions:

1. What are the hottest industries for startups right now?
2. Do machine learning startups get more press than fintech startups?
3. What is the startup segment with most acquisitions?

Before moving on you should read the full three part blog series starting 
with the [first post](https://blog.monkeylearn.com/filtering-startup-news-machine-learning/).

## Tools You Need to Setup

1. GNU/Linux Workstation 
2. Python 2.7
3. Pip
4. virtualenv
5. scrapy
6. git
7. A free account on MonkeyLearn.com

## How to Start?

If you want to know more about the scrapers you should clone the 
[example project](https://github.com/monkeylearn/startup-news-analysis). 
After you clone the project you should read their README.md. You'll use
their project to scrape the three news sites for data. If you prefer not
to scrape the data yourself then grab the pre-downloaded CSV files from
the data directory in this project.

Once you've the data, navigate to MonkeyLearn.com and follow the blog 
post.

Authors: Adam M Dutko

